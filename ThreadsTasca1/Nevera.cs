﻿using System;

namespace ThreadsTasca1
{
    class Nevera
    {
        int numNestea;
        
        public Nevera(int startingNesteas)
        {
            numNestea = startingNesteas;
        }

        public void FillUp(string name)
        {
            Random ran = new Random();

            int addedNesteas = ran.Next(0, 7); //El Max no se incluye, por lo que es de 0 a 6
            numNestea += addedNesteas;

            if (numNestea > 9)
            {
                numNestea = 9; //Si se pasa se establece como maximo
                Console.WriteLine(name + " ha intentado llenar la nevera por encima de sus posibilidades, añadiendo " + addedNesteas + " latas de Nestea");

            } else
            {
                //No hacia falta, pero por detallito
                switch (addedNesteas)
                {
                    case 0:
                        Console.WriteLine(name + " no añade ninguna lata");
                        break;
                    case 1:
                        Console.WriteLine(name + " ha llenado la nevera con " + addedNesteas + " lata de Nestea"); //lata
                        break;
                    default:
                        Console.WriteLine(name + " ha llenado la nevera con " + addedNesteas + " latas de Nestea"); //lataS
                        break;
                }
            }
                
            Display();
        }

        public void Drink(string name)
        {
            Random ran = new Random();

            int drunkNesteas = ran.Next(0, 7);

            int aux = numNestea;
            numNestea -= drunkNesteas;

            if (numNestea < 0)
            {
                numNestea = 0;
                if (aux == 0) Console.WriteLine("No quedan Nesteas para " + name + " :(");
                else Console.WriteLine(name + " no tiene suficiente Nestea para beber las " + drunkNesteas + " latas que queria. Se tendra que conformar con " + aux);
            }
            else
            {
                switch (drunkNesteas)
                {
                    case 0:
                        Console.WriteLine(name + " no se bebe ninguna lata");
                        break;
                    case 1:
                        Console.WriteLine(name + " ha bebido " + drunkNesteas + " lata de Nestea");
                        break;
                    default:
                        Console.WriteLine(name + " ha bebido " + drunkNesteas + " latas de Nestea");
                        break;
                }
            }
            
            Display();
        }

        public void Display()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Quedan " + numNestea + " latas");
            Console.ResetColor();
        }
    }
}
