﻿using System;
using System.Threading;

namespace ThreadsTasca1
{
    class Program
    {
        static void Main()
        {
            //Ex1();
            Ex2();
        }

        static void Ex1()
        {
            Thread t1 = new Thread(() => WriteWihDelay("Hi havia una vegada un gat"));
            
            Thread t2 = new Thread(() => {
                t1.Join();
                WriteWihDelay("En un lugar de la Mancha");           
            }
            );
            
            Thread t3 = new Thread(() => {
                t2.Join();
                WriteWihDelay("Once upon a time in the west");             
            });

            t1.Start();
            t2.Start();
            t3.Start();
        }

        static void WriteWihDelay(string frase)
        { 
            string[] separateWords = frase.Split(" "); //Hace un array separando las palabras por cada espacio
            
            foreach (string word in separateWords)
            {
                Console.Write(word + " "); //Escribe una palabra y se espera X tiempo
                Thread.Sleep(2000); //2000 ms = 2s 
            }

            Console.WriteLine(); //Hace un salto de linea para que el siguiente escriba debajo
        }

        static void Ex2()
        {
            Nevera nevera = new Nevera(6);

            Thread fill = new Thread(() => nevera.FillUp("Carlos (Yo)"));
            Thread drink1 = new Thread(() =>
            {
                fill.Join();
                nevera.Drink("Carlos (Profe)");
            });
            Thread drink2 = new Thread(() =>
            {
                drink1.Join();
                nevera.Drink("Edu");
            });
            Thread drink3 = new Thread(() =>
            {
                drink2.Join();
                nevera.Drink("Estibaliz");
            });

            Thread[] threads = new Thread[] { fill, drink1, drink2, drink3 };

            foreach (Thread thread in threads) thread.Start();
        }
    }
}
